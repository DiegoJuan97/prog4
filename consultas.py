from base import Session
from actor import Actor
from contacto import Contacto
from pelicula import Pelicula
from datetime import date

session = Session()

print ("Nueva pelicula: ")
titulo = input("Titulo : " )
lanzamiento = input("Lanzamiento: ")

nueva_pelicula = Pelicula(titulo,lanzamiento)
session.add(nueva_pelicula)
session.commit()

print("Nuevo Actor")
nombre = input("Nombre: ")
nacimiento = input("Nacimiento: ")
nuevo_actor = Actor(nombre,nacimiento)
session.add(nuevo_actor)
session.commit()


peliculas = session.query(Pelicula).all()

if len(peliculas) == 0:
    print("No hay registro de peliculas")
else:
        print("Todas las peliculas:")
        for pelicula in peliculas:
            print(pelicula.titulo + "fue lanzado en "+ str(pelicula.lanzamiento))

peliculas = session.query(Pelicula).filter(Pelicula.lanzamiento > date(2012, 1 , 1)).all 
print("\n Peliculas Recientes")
if len(peliculas) == 0:
    print("No hay peliculas")
else:
    for pelicula in peliculas:
     print(pelicula.titulo + "fue lanzada posterior a 2012")

peliculas_keanu = session.query(Pelicula).join(Actor, Pelicula.actor).filter(Actor.nombre == 'Keanu Reeves').all()
for pelicula in peliculas_keanu:
    print('\nKeanu ha participado en '+ pelicula.titulo)
