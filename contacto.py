from sqlalchemy import Column,String,Integer,ForeignKey
from sqlalchemy.orm import relationship

from base import Base  
class Contacto(Base):
    __tablename__ = 'contacto'
    id= Column(Integer, primary_key = True)
    telefono = Column(String(20))
    direccion = Column(String(150))
    id_actor = Column(Integer,ForeignKey('actor.id'))
    actor = relationship('Actor',backref='contacto')
    
    def __init__(self,telefono,direccion,actor):
        self.telefono = telefono
        self.direccion = direccion
        self.actor = actor