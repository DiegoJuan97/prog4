#Parte 3 mariabd
from sqlalchemy import Column,String,Integer,Date 
from base import Base

class Actor(Base):
    __tablename__ = "actor"

    id = Column(Integer , primary_key= true )
    nombre = Column(String(50))
    nacimiento = Column(Date) 

    def __init__ (self, nombre, nacimiento) :
        self.nombre = nombre
        self.nacimiento = nacimiento
    