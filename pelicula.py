#Parte 2 de mariabd
from sqlalchemy import Column,String,Integer,Date,Foreignkey,Table 
from sqlalchemy.orm import relationship
from base import base

actor_participa_pelicula = table(
    'peliculas_actores',base.metadata,
    Column('id_pelicula', integer, foreignkey('pelicula.id')),
    Column('id_actor', integer, foreignkey('actor.id')),

)

class Pelicula(base):
    __tablename__='pelicula'

    id = Column(Integer.primary_key = true)
    titulo = Column(string(50))
    lanzamiento = Column(date)

    def __init__(self,titulo,lanzamiento):
        self.titulo = titulo
        self.lanzamiento = lanzamiento