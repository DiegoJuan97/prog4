import sqlite3
db= sqlite3.connect('Zhillas')
cursor = db.cursor()
cursor.execute('''
CREATE TABLE IF NOT EXISTS CLIENTE (cedula TEXT PRIMARY KEY,
nombre TEXT)
''')
db.commit()
cedula = input("Cedula: ")
nombre = input("Nombre: ")
cursor.execute('''
INSERT INTO CLIENTE(cedula, nombre)
VALUES(?,?)
''', (cedula, nombre))
db.commit()
cursor.execute('''SELECT * FROM CLIENTE ''')
for row in cursor:
    print("Nombre: {1}    Cedula: {0}". format(row[0], row[1]))
cursor.execute('''SELECT COUNT (*) FROM CLIENTE ''')
conteo = cursor.fetchone()
print("El numero total de clientes es " + str(conteo[0]))
#cursor.execute('''DROP TABLE CLIENTE ''')
#db.commit()
db.close()